#ifndef FORTUNA_DO_TASK_H
#define FORTUNA_DO_TASK_H

#include <chrono>
#include <functional>
#include <mutex>
#include <thread>

namespace fortuna {

class DoTask {
private:
	std::recursive_timed_mutex mtx_do_sleep;
	std::thread th;

public:
	DoTask() noexcept = default;
	~DoTask() noexcept;

	auto thread_pls(const std::chrono::seconds& interval,
					std::function<void()> callback) -> void;
	auto die_pls() -> void;

}; // class DoTask

} // namespace fortuna

#endif // FORTUNA_DO_TASK_H
