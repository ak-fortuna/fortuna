#ifndef FORTUNA_ENTROPY_SRC_H
#define FORTUNA_ENTROPY_SRC_H

#include "event_adder_impl.h"

#include <memory>

namespace fortuna {
namespace accumulator {

class EntropySrc {
private:
	std::unique_ptr<EventAdderImpl> ea_impl;

public:
	virtual void event(accumulator::EventAdderImpl& adder) = 0;

	static constexpr const std::size_t max_event_length{32};

	EntropySrc() = default;
	EntropySrc(const EntropySrc&) = delete;
	EntropySrc& operator=(const EntropySrc&) = delete;

protected:
	virtual ~EntropySrc() noexcept = default;
};

} // namespace accumulator
} // namespace fortuna
#endif // FORTUNA_ENTROPY_SRC_H
