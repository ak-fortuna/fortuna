#ifndef FORTUNA_EVENT_ADDER_H
#define FORTUNA_EVENT_ADDER_H

#include <vector>

namespace fortuna {
namespace accumulator {

class EventAdder {
public:
	virtual void add(const std::vector<char>& e) = 0;

protected:
	virtual ~EventAdder() noexcept = default;
};

} // namespace accumulator
} // namespace fortuna
#endif // FORTUNA_EVENT_ADDER_H
