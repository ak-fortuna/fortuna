#ifndef FORTUNA_EVENT_ADDER_IMPL_H
#define FORTUNA_EVENT_ADDER_IMPL_H

#include "event_adder.h"
#include "pool.h"

#include <fmt/core.h>

#include <cstdint>
#include <exception>
#include <memory>
#include <stdexcept>

namespace fortuna {
namespace accumulator {

class EventAdderImpl final : public EventAdder {
private:
	uint8_t pool_id;
	uint64_t where_to{0};
	unsigned int _source_id;
	std::shared_ptr<
		std::array<accumulator::Pool, accumulator::Pool::NUM_OF_POOLS>>
		_pools;

public:
	EventAdderImpl(
		const unsigned int source_id,
		std::shared_ptr<std::array<accumulator::Pool,
								   accumulator::Pool::NUM_OF_POOLS>> pools) {
		this->_source_id = source_id;
		if (pools) {
			this->_pools = pools;
		}
		else {
			throw std::runtime_error(
				"[!] adder: pools pointer no longer valid\n");
		}
		this->pool_id = 0;
	}

	void add(const std::vector<char>& event) override {
		try {
			this->pool_id = (this->where_to) % 32;
			fmt::print(stderr, "[i] add to pool {}\n", this->pool_id);
			int ret{this->_pools->at(this->pool_id)
						.add_entropy(this->_source_id, event)};
			if (ret == 1) {
				throw std::runtime_error(
					"\t[!] event_adder: error adding event!\n");
			}
			// FIXME: this WILL overflow, too
			++where_to;
		}
		catch (std::exception& e) {
			throw;
		}
	}
};

} // namespace accumulator
} // namespace fortuna
#endif // FORTUNA_EVENT_ADDER_IMPL_H
