#include "fortuna.h"

#include <fmt/core.h>

#include <chrono>
#include <csignal>
#include <cstdlib>
#include <exception>
#include <memory>
#include <thread>
#include <unistd.h>


namespace chance {
static constexpr const bool AUTOSTOP{false};
const std::unique_ptr<fortuna::Fortuna> f{std::make_unique<fortuna::Fortuna>()};

auto sigint_handler(int sig_n) -> void {
	assert(sig_n == 2);
	std::atomic<unsigned int> madness{100000};
	try {
		constexpr auto madmsg{"HAHA no not ending on SIGINT, send a SIGTERM"};
		while (madness-- > 0) {
			fmt::print(stderr, "{:<{}}\n", madmsg, 16);
		}
	}
	catch (std::exception& e) {
		fmt::print(
			stderr, "[!] bailing, something (bad) happened: {}\n", e.what());
		throw;
	}
}

auto sigterm_handler(int sig_n) -> void {
	assert(sig_n == 15);
	try {
		constexpr auto news{" BREAKING NEWS "};
		fmt::print(stderr, "\n\n{:*^{}}\n\n", news, 79);
		fmt::print(stderr,
				   "[i] main: received signal {} (SIGTERM)\n\tpid: {}\n\twill "
				   "attempt to stop\n",
				   sig_n,
				   getpid());
		if (f) {
			f->stop_running();
		}
		else {
			throw("fortuna pointer is invalid");
		}
	}
	catch (std::exception& e) {
		fmt::print(
			stderr, "[!] bailing, something (bad) happened: {}\n", e.what());
		throw;
	}
}

auto lil_demo() -> void {
	using std::chrono_literals::operator""ms;
	chance::f->random_data(4);
	std::this_thread::sleep_for(120ms);
	chance::f->random_data(3);
	chance::f->random_data(3);
	std::this_thread::sleep_for(10ms);
	chance::f->random_data(3);
	std::this_thread::sleep_for(5000ms);
	chance::f->random_data(30);
	std::this_thread::sleep_for(5000ms);
	chance::f->random_data(30);
	std::this_thread::sleep_for(2000ms);
	chance::f->random_data(1000);
}
} // namespace chance


int main() {
	fmt::print(stderr, "[*] doing evil stuff\n");

	signal(SIGTERM, chance::sigterm_handler);
	signal(SIGINT, chance::sigint_handler);
	signal(SIGUSR1, SIG_IGN);
	signal(SIGUSR2, SIG_IGN);

	try {
		using std::chrono_literals::operator""ms;
		// this may fail, p0 probaby has not collected enough entropy by now
		chance::f->random_data(4); // number of bytes requested

		std::this_thread::sleep_for(7777ms); // enough time for p0 == 64
		chance::f->moar_random_data(1000000000000); // 1000 GB --> for dieharder

		if (chance::AUTOSTOP) {
			auto err{chance::f->stop_running()};
			if (err) {
				fmt::print(stderr, "[!] ERROR: failed to stop Fortuna!\n");
				throw std::runtime_error("failed to stop Fortuna nicely");
			}
		}
	}
	catch (std::exception& e) {
		fmt::print(stderr, "[!] exiting due to \"{}\"\n", e.what());
		exit(EXIT_FAILURE);
	}

	return 0;
}
