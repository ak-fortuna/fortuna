#ifndef FORTUNA_POOL_H
#define FORTUNA_POOL_H

#include "util.h"

#include <mutex>

namespace fortuna {
namespace accumulator {

class Pool {
public:
	static constexpr const uint8_t NUM_OF_POOLS{32};

	Pool() noexcept {}
	Pool(const Pool&) = delete; // no copy
	Pool(Pool&) = delete;
	Pool& operator=(const Pool&) = delete;
	~Pool() noexcept = default;

	auto set_id(const unsigned int& id) noexcept -> void;

	auto get_id() const noexcept -> unsigned int;

	auto initialize_pool(const unsigned int& id) -> void;

	// add entropy contained in a random event of 1 to 32 bytes
	auto add_entropy(const unsigned int& source, const std::vector<char>& event)
		-> int;

	auto get_s_byte_count() const -> uint64_t;

	auto get_s() const -> std::string;

	auto clear_pool() -> void;

protected:
	auto append_s(const std::string& entropy_s) -> void;

private:
	unsigned int pool_id{0};
	mutable std::string s{""};
	uint64_t size{0};
	std::mutex mtx; // mutex for write locks
	mutable std::recursive_mutex ro_mtx_s; // mtx for readers of s
	std::once_flag init;

}; // class Pool

} // namespace accumulator
} // namespace fortuna

#endif // FORTUNA_POOL_H
