#ifndef FORTUNA_SEED_FILE_MANAGER_H
#define FORTUNA_SEED_FILE_MANAGER_H

#include "accumulator.h"
#include "do_task.h"

#include <atomic>
#include <chrono>
#include <mutex>
#include <string>

namespace fortuna {

class SeedFileManager {
public:
	struct conf {
		// std::chrono::minutes write_interval{10};
		// 10 minutes (as the standard recommends) is a lot, go with 10s
		const std::chrono::seconds write_interval{10};
		std::string seed_f_path{"/var/tmp/.fortuna.seed"};
		std::size_t seed_f_length{64};

		conf(){}
	};

	auto is_job_running() const -> bool;
	auto do_stuff() -> void;
	auto get_write_interval() const -> std::chrono::seconds;

	explicit SeedFileManager(std::shared_ptr<fortuna::accumulator::Accumulator>
								 accumulator) noexcept;
	~SeedFileManager() noexcept;

protected:
	auto set_job_running(bool) -> void;

private:
	const conf config;
	mutable std::recursive_mutex mtx;
	std::atomic<bool> IS_RUNNING{false};
	DoTask do_task;
	std::shared_ptr<fortuna::accumulator::Accumulator> _p_accumulator;

	auto seed_file_exists() const -> bool;
	auto write_seed_file() -> void;
	auto update_seed_file() -> void;

}; // class SeedFileManager

} // namespace fortuna

#endif // FORTUNA_SEED_FILE_MANAGER_H
