#ifndef FORTUNA_URANDOM_ENTROPY_SRC_H
#define FORTUNA_URANDOM_ENTROPY_SRC_H

#include "entropy_src.h"
#include "event_adder_impl.h"

#include <fmt/core.h>

#include <cstddef>
#include <cstdint>
#include <fstream>
#include <mutex>
#include <vector>

namespace fortuna {
namespace accumulator {

class UrandomEntropySrc : public EntropySrc {
private:
	std::vector<char> bytes{0};
	std::mutex mtx;

public:
	auto event(accumulator::EventAdderImpl& adder) -> void override;

	auto urandom_entropy_src_service() -> int;

	UrandomEntropySrc();
	~UrandomEntropySrc() noexcept;
};

} // namespace accumulator
} // namespace fortuna
#endif // FORTUNA_URANDOM_ENTROPY_SRC_H
